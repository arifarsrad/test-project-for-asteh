# Test Project for АСТЕХ" 

## Description

It's a handbook with users. Main features:

## How to run frontend

### Project variables
- `PORT`. Port of the frontend server, defaults to `3000`;
- `HOSTNAME`. Host of the frontend server, defaults to `"localhost"`;
- `PUBLIC_BACKEND_API_URL`. Seen to the browser, defaults to `"/api"` (relative path);

### Production

1. Set environment variables
2. Install dependencies 

```shell script
npm i
```

3. Build a project into `.next` directory

```shell script
npm run build
```

4. Start server using `.next` directory

```shell script
npm run start
```

### Development

1. Set environment variables
2. Install dependencies

```shell script
npm i
```

3. Start development server with hot module
replacement (i.e. fast-refresh)

```shell script
npm run dev
```
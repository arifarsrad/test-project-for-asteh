const path = require('path')

module.exports = {
	webpack: (
		config,
		{ buildId, dev, isServer, defaultLoaders, nextRuntime, webpack }
	) => {
		config.resolve = {
			...config.resolve,
			alias: {
				...config.resolve.alias,
				Api: path.resolve(__dirname, 'src/api'),
				Components: path.resolve(__dirname, 'src/components'),
				Constants: path.resolve(__dirname, 'src/constants'),
				Styles: path.resolve(__dirname, 'src/styles'),
				Utils: path.resolve(__dirname, 'src/utils')
			}
		}

		// Important: return the modified config
		return config
	}
}
